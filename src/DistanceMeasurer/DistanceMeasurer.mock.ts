import { Coordinates } from './strategies';

export const acceptedMeasurementError = 1;

export const validCoordinates: Coordinates[] = [
  {
    latitude: 53.11368755,
    longitude: 14.03961756,
  },
  {
    latitude: 53.30853484,
    longitude: 15.73244752,
  },
  {
    latitude: 51.56055982,
    longitude: 16.55563876,
  },
];

export const validDistances: number[] = [
  79.7,
  178.1,
  236.7,
];
