export interface ClientData {
  id: string;
  latitude: number;
  longitude: number;
}
