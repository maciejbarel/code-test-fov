import { ClientData } from './ClientData';
import { InputParser } from './InputParser';
import { validClientData, validInput, invalidInputs } from './InputParser.mock';

describe('InputParser', () => {
  let inputParser: InputParser;

  beforeEach(() => {
    inputParser = new InputParser();
  });

  it('should parse client record from given string', () => {
    const output: ClientData = inputParser.parseClientDataFromString(validInput);

    expect(output.id).toEqual(validClientData.id);
    expect(output.latitude).toEqual(validClientData.latitude);
    expect(output.longitude).toEqual(validClientData.longitude);
  });

  invalidInputs.byId.forEach((input: string) => {
    it(`should throw error for input with invalid id: "${input}"`, () => {
      expect(() => inputParser.parseClientDataFromString(input))
        .toThrow();
    });
  });

  invalidInputs.byLatitude.forEach((input: string) => {
    it(`should throw error for input with invalid latitude: "${input}"`, () => {
      expect(() => inputParser.parseClientDataFromString(input))
        .toThrow();
    });
  });

  invalidInputs.byLongitude.forEach((input: string) => {
    it(`should throw error for input with invalid longitude: "${input}"`, () => {
      expect(() => inputParser.parseClientDataFromString(input))
        .toThrow();
    });
  });
});
