import { ReadLine } from 'readline';

import { ClientData, InputParser } from './InputParser';
import { InputReader } from './InputReader';
import { centralPoint, Coordinates, DistanceMeasurer } from './DistanceMeasurer';

const maximumAcceptableDistance = 100;

class App {
  private inputParser: InputParser;
  private inputReader: InputReader;
  private distanceMeasurer: DistanceMeasurer;

  private static printResults(results: string[]): void {
    console.log('Results:');
    results.forEach((result: string) => console.log(result));
  }

  private static isDistanceCorrect(distance: number): boolean {
    return distance <= maximumAcceptableDistance;
  }

  constructor() {
    this.inputParser = new InputParser();
    this.inputReader = new InputReader();
    this.distanceMeasurer = new DistanceMeasurer(centralPoint);
  }

  run(): void {
    const stream: ReadLine = this.inputReader.getReadline();
    const results: string[] = [];

    stream.on('line', (line: string) => {
      try {
        const clientData: ClientData = this.inputParser.parseClientDataFromString(line);
        const coordinates: Coordinates = this.getCoordinatesFromClientData(clientData);
        const distance: number = this.distanceMeasurer.measureDistanceToCentralPoint(coordinates);

        if (App.isDistanceCorrect(distance)) {
          results.push(clientData.id);
        }
      } catch (error) {
        console.log('Invalid input, see line content:', line);
      }
    }).on('close', () => {
      results.sort();
      App.printResults(results);
    });
  }

  private getCoordinatesFromClientData(data: ClientData): Coordinates {
    const coordinates: Coordinates = {
      latitude: data.latitude,
      longitude: data.longitude,
    };

    return coordinates;
  }
}

const app = new App();

app.run();
