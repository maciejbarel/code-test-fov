import { ReadStream } from 'fs';

export interface InputReaderStrategy {
  getReadableStream(): ReadStream;
}
