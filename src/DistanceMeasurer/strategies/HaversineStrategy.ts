import { Coordinates, DistanceMeasurerStrategy } from './DistanceMeasurerStrategy';

export class HaversineStrategy implements DistanceMeasurerStrategy {
  private static pi = Math.PI;
  private static earthRadius = 6371;

  measureDistance(firstCoordinates: Coordinates, secondCoordinates: Coordinates): number {
    const firstCoordinatesInRadians = this.convertCoordinatesToRadians(firstCoordinates);
    const secondCoordinatesInRadians = this.convertCoordinatesToRadians(secondCoordinates);

    return this.haversineFormula(firstCoordinatesInRadians, secondCoordinatesInRadians);
  }

  private convertCoordinatesToRadians(coordinates: Coordinates): Coordinates {
    const latitude = this.degreesToRadians(coordinates.latitude);
    const longitude = this.degreesToRadians(coordinates.longitude);

    return {
      latitude,
      longitude,
    };
  }

  private degreesToRadians(degrees: number): number {
    const radians = degrees / 180 * HaversineStrategy.pi;
    return radians;
  }

  private haversineFormula(firstCoordinates: Coordinates, secondCoordinates: Coordinates): number {
    const latitudeDelta = firstCoordinates.latitude - secondCoordinates.latitude;
    const longitudeDelta = firstCoordinates.longitude - secondCoordinates.longitude;

    const result = (
      2 * HaversineStrategy.earthRadius * Math.asin(
        Math.sqrt(
          this.haversine(latitudeDelta) + Math.cos(firstCoordinates.latitude) * Math.cos(secondCoordinates.latitude) * this.haversine(longitudeDelta)
        )
      )
    );

    return result;
  }

  private haversine(value: number): number {
    return Math.sin(value / 2) ** 2;
  }
}
