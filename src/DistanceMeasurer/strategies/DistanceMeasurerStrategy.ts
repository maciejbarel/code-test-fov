export interface Coordinates {
  latitude: number;
  longitude: number;
}

export interface DistanceMeasurerStrategy {
  measureDistance(firstCoordinates: Coordinates, secondCoordinates: Coordinates): number;
}
