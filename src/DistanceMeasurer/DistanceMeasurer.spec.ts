import { centralPoint, DistanceMeasurer } from './DistanceMeasurer';
import { acceptedMeasurementError, validCoordinates, validDistances } from './DistanceMeasurer.mock';

describe('InputParser', () => {
  let distanceMeasurer: DistanceMeasurer;

  beforeEach(() => {
    distanceMeasurer = new DistanceMeasurer(centralPoint);
  });

  validCoordinates.forEach((coordinates: Coordinates, index: number) => {
    it('should measure correctly the distance between given point and the central point', () => {
      const minimumAcceptedValue = validDistances[index] - acceptedMeasurementError;
      const maximumAcceptedValue = validDistances[index] + acceptedMeasurementError;

      const measuredDistance = distanceMeasurer.measureDistanceToCentralPoint(coordinates);

      expect(measuredDistance).toBeGreaterThanOrEqual(minimumAcceptedValue);
      expect(measuredDistance).toBeLessThanOrEqual(maximumAcceptedValue);
    });
  });
});
