import readline from 'readline';

import { InputReaderStrategy, ReadFromStaticFileStrategy } from './strategies';

export class InputReader {
  private strategy: InputReaderStrategy;

  getReadline(): readline.ReadLine {
    const input = this.strategy.getReadableStream();

    return readline.createInterface({
      input,
    });
  }

  constructor() {
    this.strategy = new ReadFromStaticFileStrategy();
  }
}
