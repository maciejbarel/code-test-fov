import { Coordinates, DistanceMeasurerStrategy, HaversineStrategy } from './strategies';

export const centralPoint: Coordinates = {
  latitude: 52.493256,
  longitude: 13.446082,
};

export class DistanceMeasurer {
  private centralPoint: Coordinates;
  private strategy: DistanceMeasurerStrategy;

  constructor(centralPoint: Coordinates) {
    this.centralPoint = centralPoint;
    this.strategy = new HaversineStrategy();
  }

  measureDistanceToCentralPoint(point: Coordinates): number {
    return this.strategy.measureDistance(this.centralPoint, point);
  }
}
