export { centralPoint, DistanceMeasurer } from './DistanceMeasurer';
export { Coordinates } from './strategies/DistanceMeasurerStrategy';
