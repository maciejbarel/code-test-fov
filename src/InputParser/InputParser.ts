import { ClientData } from './ClientData';

enum propertyIndex {
  id = 0,
  latitude = 1,
  longitude = 2,
}

const latitudeRegex = /^-?\d{1,2}\.\d+$/;
const longitudeRegex = /^-?\d{1,3}\.\d+$/;


export class InputParser {
  parseClientDataFromString(input: string): ClientData {
    const inputArguments = input.split(',');

    const id = this.parseId(inputArguments[propertyIndex.id]);
    const latitude = this.parseLatitude(inputArguments[propertyIndex.latitude]);
    const longitude = this.parseLongitude(inputArguments[propertyIndex.longitude]);

    return {
      id,
      latitude,
      longitude,
    };
  }

  private parseId(value: string): string {
    const rawId = this.cleanValue(value);
    const validId = this.validateId(rawId);

    return validId;
  }

  private parseLatitude(value: string): number {
    const rawLatitude = this.cleanValue(value);
    const validLatitude = this.validateLatitude(rawLatitude);

    return validLatitude;
  }

  private parseLongitude(value: string): number {
    const rawLongitude = this.cleanValue(value);
    const validLongitude = this.validateLongitude(rawLongitude);

    return validLongitude;
  }

  private cleanValue(rawValue: string): string {
    const value = this.getValueAfterColon(rawValue);

    return value.trim();
  }

  private getValueAfterColon(input: string): string {
    if (input.lastIndexOf(':') === -1) {
      throw new Error('Colon is missing');
    }

    const splitInput = input.split(':');

    return splitInput[splitInput.length - 1];
  }

  private validateId(value: string): string {
    if (!Boolean(value)) {
      throw new Error('Invalid id');
    }

    return value;
  }

  private validateLatitude(rawLatitude: string): number {
    if (!latitudeRegex.test(rawLatitude)) {
      throw new Error('Invalid latitude');
    }

    const parsedLatitude = parseFloat(rawLatitude);
    if (parsedLatitude > 90 || parsedLatitude < -90) {
      throw new Error('Invalid longitude');
    }

    return parsedLatitude;
  }

  private validateLongitude(rawLongitude: string): number {
    if (!longitudeRegex.test(rawLongitude)) {
      throw new Error('Invalid longitude');
    }

    const parsedLongitude = parseFloat(rawLongitude);
    if (parsedLongitude > 180 || parsedLongitude < -180) {
      throw new Error('Invalid longitude');
    }

    return parsedLongitude;
  }
}
