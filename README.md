# Code Test for Future of Voice

This project was prepared as a code test for Future of Voice. The goal of the app is to find all the clients in range of 100 km from the central point and print out their ids in alphabetical order.

## Installation

Run `npm i` to install required dependencies.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Start

Install dependencies and build the project. After that, run `npm start` to run the app.

## Running unit tests

Run `npm test` to execute the unit tests via [Jest](https://facebook.github.io/jest/).
