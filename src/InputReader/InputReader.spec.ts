import readline from 'readline';

import { InputReader } from './InputReader';

describe('InputReader', () => {
  let inputReader: InputReader;

  beforeEach(() => {
    inputReader = new InputReader();
  });

  it('should get a readline instance', () => {
    const output = inputReader.getReadline();

    expect(output).toBeDefined();
  });
});
