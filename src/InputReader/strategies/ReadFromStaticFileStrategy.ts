import { ReadStream, createReadStream } from 'fs';
import path from 'path';

import { InputReaderStrategy } from './InputReaderStrategy';

export class ReadFromStaticFileStrategy implements InputReaderStrategy {
  private pathToStaticFile = path.join(__dirname, '../../../static/customers.txt');

  getReadableStream(): ReadStream {
    return createReadStream(this.pathToStaticFile);
  }
}