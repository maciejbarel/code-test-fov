import { ClientData } from './ClientData';

export const validInput = 'id: bbccc9e8-5cee-439a-a00c-0088b88bc327, lat: 53.11368755, long:14.03961756,';
export const validClientData: ClientData = {
  id: 'bbccc9e8-5cee-439a-a00c-0088b88bc327',
  latitude: 53.11368755,
  longitude: 14.03961756,
};

export const invalidInputs = {
  byId: [
    'bbccc9e8-5cee-439a-a00c-0088b88bc327, lat: 53.11368755, long:14.03961756,',
    'id:, lat: 53.11368755, long:14.03961756,',
    'id: , lat: 53.11368755, long:14.03961756,',
    'id, lat: 53.11368755, long:14.03961756,',
  ],
  byLatitude: [
    'id: bbccc9e8-5cee-439a-a00c-0088b88bc327, lat: 53.113INVALID68755, long:14.03961756,',
    'id: bbccc9e8-5cee-439a-a00c-0088b88bc327, lat: INVALID53.11368755, long:14.03961756,',
    'id: bbccc9e8-5cee-439a-a00c-0088b88bc327,53.11368755, long:14.03961756,',
    'id: bbccc9e8-5cee-439a-a00c-0088b88bc327, lat: 53.11368755INVALID, long:14.03961756,',
    'id: bbccc9e8-5cee-439a-a00c-0088b88bc327, lat: 93.11368755, long:14.03961756,',
  ],
  byLongitude: [
    'bbccc9e8-5cee-439a-a00c-0088b88bc327, lat: 53.11368755, long:14.039INVALID61756,',
    'bbccc9e8-5cee-439a-a00c-0088b88bc327, lat: 53.11368755, long:INVALID14.03961756,',
    'bbccc9e8-5cee-439a-a00c-0088b88bc327, lat: 53.11368755, long:14.03961756,',
    'bbccc9e8-5cee-439a-a00c-0088b88bc327, lat: 53.11368755, long:14.03961756INVALID,',
    'bbccc9e8-5cee-439a-a00c-0088b88bc327, lat: 53.11368755, long:2394.03961756,',
  ],
};
